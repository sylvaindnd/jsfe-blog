<?php

use Router\Router;

require '../vendor/autoload.php';

define('VIEWS', dirname(__DIR__). DIRECTORY_SEPARATOR . 'views'. DIRECTORY_SEPARATOR);

define('SCRIPTS', dirname($_SERVER['SCRIPT_NAME']) . DIRECTORY_SEPARATOR);

$router = new Router($_GET['url']);

$router->get('/', 'BlogController@index');
$router->post('/login', 'BlogController@login');
$router->get('/login', 'BlogController@loginView');

try{
    $router->run();

} catch(NotFoundException $e){
    echo $e->error404();
}