<?php

namespace App\Controllers;
use App\models\Model;

class BlogController extends Controller{

    public function index()
    {
        return $this->view('index');
    }

    public function login()
    {
        session_start();
        $_SESSION['user']=false;
        
        $login = $_POST['inputUsername'];
        $password = $_POST['inputPassword'];
        
        $user = new Model; 
        $check = $user->user_logged($login,$password);

        if($check){
            $_SESSION['user']=true;
            return $this->view('index');
        }else{
            $message = "Wrong password or login";
            return $this->view('login', compact("message"));
        } 

       

    }

    public function loginView()
    {
        return $this->view('login');
    }
}   